import java.util.Scanner;

public class TicTacToeGame {
    
    public static void main(String[] args) {
        System.out.println("Welcome to Tic Tac Toe! By Fabilo Silva");
        
        Board board = new Board();
        boolean gameOver = false;
        int player = 1;
        Square playerToken = Square.X;
        
        while (!gameOver) 
		{
            System.out.println(board);
            
            if (player == 1) 
			{
                playerToken = Square.X;
            } 
			else 
			{
                playerToken = Square.O;
            }
            //using the %d syntax, we'll be adding the variable to be substituted inside the print statement preventing us from having one loop for each player.
            int row, col;
            Scanner scanner = new Scanner(System.in);
            System.out.printf("Player %d, enter a row (0-2): ", player);
            row = scanner.nextInt();
            System.out.printf("Player %d, enter a column (0-2): ", player);
            col = scanner.nextInt();
            
            boolean placed = board.placeToken(row, col, playerToken);
            while (!placed) 
			{
                System.out.println("Invalid input. Try again.");
                System.out.printf("Player %d, enter a row (0-2): ", player);
                row = scanner.nextInt();
                System.out.printf("Player %d, enter a column (0-2): ", player);
                col = scanner.nextInt();
                placed = board.placeToken(row, col, playerToken);
            }
            
            if (board.checkIfWinning(playerToken)) 
			{
                System.out.printf("Player %d wins!\n", player);
                gameOver = true;
            } 
			else if (board.checkIfFull()) 
			{
                System.out.println("It's a tie!");
                gameOver = true;
            } 
			else 
			{
                player++;
                if (player > 2) //after playing one round, the number inside player will be incremented and will return to the player 1.
				{
                    player = 1;
                }
            }
        }
    }
}
