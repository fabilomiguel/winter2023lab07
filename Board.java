public class Board {
    private Square[][] tictactoeBoard;

    public Board() {
        tictactoeBoard = new Square[3][3];
        for (int row = 0; row < 3; row++) 
		{
            for (int col = 0; col < 3; col++) 
			{
                tictactoeBoard[row][col] = Square.BLANK;
            }
        }
    }

    public String toString() 
	{
        String boardString = "";
        for (int i = 0; i < 3; i++) 
		{
            for (int j = 0; j < 3; j++) 
			{
                boardString += tictactoeBoard[i][j] + " ";
            }
            boardString += "\n";
        }
        return boardString;
    }

    public boolean placeToken(int row, int col, Square playerToken) 
	{
        if (row < 0 || row > 2 || col < 0 || col > 2) 
		{
            return false;
        }
        if (tictactoeBoard[row][col] == Square.BLANK) 
		{
            tictactoeBoard[row][col] = playerToken;
            return true;
        } 
		else 
		{
            return false;
        }
    }

    public boolean checkIfFull() 
	{
        for (int i = 0; i < 3; i++) 
		{
            for (int j = 0; j < 3; j++) 
			{
                if (tictactoeBoard[i][j] == Square.BLANK)
				{
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkIfWinningHorizontal(Square playerToken) 
	{
        for (int i = 0; i < 3; i++) 
		{
            if (tictactoeBoard[i][0] == playerToken &&
                    tictactoeBoard[i][1] == playerToken &&
                    tictactoeBoard[i][2] == playerToken) {
                return true;
            }
        }
        return false;
    }

    private boolean checkIfWinningVertical(Square playerToken) {
        for (int i = 0; i < 3; i++) {
            if (tictactoeBoard[0][i] == playerToken &&
                    tictactoeBoard[1][i] == playerToken &&
                    tictactoeBoard[2][i] == playerToken) {
                return true;
            }
        }
        return false;
    }

     private boolean checkIfWinningDiagonal(Square playerToken) {
        if (tictactoeBoard[0][0] == playerToken &&
            tictactoeBoard[1][1] == playerToken &&
            tictactoeBoard[2][2] == playerToken) {
            return true;
        }
        if (tictactoeBoard[0][2] == playerToken &&
            tictactoeBoard[1][1] == playerToken &&
            tictactoeBoard[2][0] == playerToken) {
            return true;
        }
        return false;
    }

    public boolean checkIfWinning(Square playerToken) {
        return checkIfWinningHorizontal(playerToken) ||
               checkIfWinningVertical(playerToken) ||
               checkIfWinningDiagonal(playerToken);
    }
}
